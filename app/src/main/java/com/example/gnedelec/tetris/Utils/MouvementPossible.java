package com.example.gnedelec.tetris.Utils;

/**
 * Created by gnedelec on 14/05/18.
 */

public interface MouvementPossible {
    public boolean canMove(Direction direction, int[][] grid);
    public boolean canRotate(int [][] grid);
}
