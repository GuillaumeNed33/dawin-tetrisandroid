package com.example.gnedelec.tetris.Utils;

/**
 * Created by gnedelec on 14/05/18.
 */

public interface Mouvement {
    public void rotate();
    public void left();
    public void right();
    public void down();
}
