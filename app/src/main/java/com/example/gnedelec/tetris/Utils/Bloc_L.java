package com.example.gnedelec.tetris.Utils;

import com.example.gnedelec.tetris.R;

/**
 * Created by gnedelec on 14/05/18.
 */

public class Bloc_L extends Bloc {

    Bloc_L() {
        super();
        int[][][] m = {
                //Default
                {
                        {3,0},
                        {3,0},
                        {3,3}
                },
                //Other rotation
                {
                        {3, 3, 3},
                        {3, 0, 0}
                },
                {
                        {3,3},
                        {0,3},
                        {0,3},
                },
                {
                        {0, 0, 3},
                        {3, 3, 3}
                },
        };
        this.setHeight(3);
        this.setWidth(2);
        this.setMatrix(m);
        this.setColor(R.drawable.bloc_l);
    }
}
