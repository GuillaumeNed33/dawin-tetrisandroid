package com.example.gnedelec.tetris.Utils;

import com.example.gnedelec.tetris.R;

/**
 * Created by gnedelec on 14/05/18.
 */

public class Bloc_Z extends Bloc {

    Bloc_Z() {
        super();
        int[][][] m = {
                //Default
                {
                        {7, 7, 0},
                        {0, 7, 7}
                },
                //Other rotation
                {
                        {0, 7},
                        {7, 7},
                        {7, 0}
                },
        };
        this.setHeight(2);
        this.setWidth(3);
        this.setMatrix(m);
        this.setColor(R.drawable.bloc_z);
    }
}
