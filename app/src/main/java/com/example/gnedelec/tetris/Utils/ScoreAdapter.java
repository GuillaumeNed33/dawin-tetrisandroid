package com.example.gnedelec.tetris.Utils;

import android.content.Context;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.gnedelec.tetris.R;

import java.util.ArrayList;
import java.util.List;

import static java.security.AccessController.getContext;

public class ScoreAdapter extends ArrayAdapter<String> {

    public ScoreAdapter(Context context, List<String> tweets) {
        super(context, 0, tweets);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        if(convertView == null){
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.score_row,parent, false);
        }

        ScoreViewHolder viewHolder = (ScoreViewHolder) convertView.getTag();
        if(viewHolder == null){
            viewHolder = new ScoreViewHolder();
            viewHolder.score = (TextView) convertView.findViewById(R.id.score);
            viewHolder.pos = (TextView) convertView.findViewById(R.id.pos);
            convertView.setTag(viewHolder);
        }

        //getItem(position) va récupérer l'item [position] de la List<Tweet> tweets
        String score = getItem(position);
        viewHolder.score.setText(score.split(";")[1]);
        viewHolder.pos.setText(score.split(";")[0]);

        return convertView;
    }

    private class ScoreViewHolder{
        public TextView score;
        public TextView pos;

    }
}
