package com.example.gnedelec.tetris.Utils;

import android.util.Log;

import java.util.Random;

public abstract class Bloc implements MouvementPossible, Mouvement {
    protected int height;
    protected int width;
    protected int[][][] matrix;
    protected int pos_i;
    protected int pos_j;
    protected int color;
    protected int rotateDirection = 0;

    Bloc() {
        this.pos_i = 4;
        this.pos_j = 0;
    }

    public static Bloc getRandomBloc() {
        Random rand = new Random();
        int randomNum = rand.nextInt((7 - 1) + 1) + 1;
        switch (randomNum) {
            case 1:
                return new Bloc_I();
            case 2:
                return new Bloc_J();
            case 3:
                return new Bloc_L();
            case 4:
                return new Bloc_O();
            case 5:
                return new Bloc_S();
            case 6:
                return new Bloc_T();
            case 7:
                return new Bloc_Z();
            default:
                return new Bloc_I();
        }
    }

    @Override
    public boolean canMove(Direction direction, int[][] grid) {
        boolean move = true;

        //Test de débordement de la grille
        switch (direction) {
            case LEFT:
                if(pos_i - 1 < 0) {
                    move = false;
                }
                break;
            case RIGHT:
                //Test de débordement de la grille
                if(pos_i + width >= grid[0].length) {
                    move = false;
                }
                break;
            case BOTTOM:
                //Test de débordement de la grille
                if(pos_j + height >= grid.length) {
                    move = false;
                }
                break;
            default:
                move = false;
                Log.i("ERR", "Direction incorrecte.");
                break;
        }

        //Test de superposition de piece
        if(move) {
            int i = 0;
            int j= 0;
            while (i < getShape().length && move) {
                j=0;
                while (j < getShape()[i].length && move) {
                    if (getShape()[i][j] != 0) {
                        switch (direction) {
                            case LEFT:
                                if(j-1 < 0 || (j-1 >= 0 && getShape()[i][j-1] == 0)) {
                                    if (grid[pos_j + i][pos_i + j - 1] != 0) {
                                        move = false;
                                    }
                                }
                                break;
                            case RIGHT:
                                if(j+1 >= getShape()[i].length || (j+1 < getShape()[i].length && getShape()[i][j+1] == 0)) { //Vérification que la case de destination est vide
                                    if (grid[pos_j + i][pos_i + j + 1] != 0) { //verification qu c'est vide dans la grille
                                        move = false;
                                    }
                                }
                                break;
                            case BOTTOM:
                                if(i+1 >= getShape().length || (i+1 < getShape().length && getShape()[i+1][j] == 0)) {
                                    if (grid[pos_j + i + 1][pos_i + j] != 0) {
                                        move = false;
                                    }
                                }
                                break;
                            default:
                                move = false;
                                Log.i("ERR", "Direction incorrecte.");
                                break;
                        }
                    }
                    j++;
                }
                i++;
            }
        }
        return move;
    }

    @Override
    public boolean canRotate(int [][] grid) {
        int height = this.matrix[(rotateDirection+1)%matrix.length].length;
        int width = this.matrix[(rotateDirection+1)%matrix.length][0].length;
        int[][] shape = this.matrix[(rotateDirection+1)%matrix.length];
        boolean canRotate = true;

        //TEST DEPASSEMENT GRILLE
        if(pos_i < 0 || pos_i + width -1 >= grid[0].length || pos_j + height-1 >= grid.length) {
            canRotate = false;
        }

        //TEST SUPERPOSITION
        if(canRotate) {
            int i = 0;
            int j= 0;
            while (i < shape.length && canRotate) {
                j=0;
                while (j < shape[i].length && canRotate) {
                    if (shape[i][j] != 0) {
                        if (grid[pos_j + i][pos_i + j] != 0) {
                            canRotate = false;
                        }
                    }
                    j++;
                }
                i++;
            }
        }
        return canRotate;
    }

    @Override
    public void rotate() {
        rotateDirection=(rotateDirection+1)%matrix.length;
        setHeight(getShape().length);
        setWidth(getShape()[0].length);
    }

    @Override
    public void left() {
        this.pos_i = this.pos_i -1;
    }

    @Override
    public void right() {
        this.pos_i = this.pos_i +1;
    }

    @Override
    public void down() {
        this.pos_j = this.pos_j + 1;
    }

    /*** GETTERS AND SETTERS ***/

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public int getPos_i() {
        return pos_i;
    }

    public void setPos_i(int pos_i) {
        this.pos_i = pos_i;
    }

    public int getPos_j() {
        return pos_j;
    }

    public void setPos_j(int pos_j) {
        this.pos_j = pos_j;
    }

    public int[][][] getMatrix() {
        return matrix;
    }

    public void setMatrix(int[][][] matrix) {
        this.matrix = matrix;
    }

    public int[][] getShape() {
        return this.matrix[rotateDirection];
    }

    public int getColor() {
        return color;
    }

    public void setColor(int color) {
        this.color = color;
    }
}
