package com.example.gnedelec.tetris;

import android.content.DialogInterface;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.gnedelec.tetris.Utils.Bloc;
import com.example.gnedelec.tetris.Utils.Direction;
import com.example.gnedelec.tetris.Utils.GridAdapter;

public class GameActivity extends AppCompatActivity {

    private static final int NB_COLONNES = 10;
    private static final int NB_LIGNES = 18;
    private static int GAME_SPEED = 650; //milliseconds
    public static final String SCORE = "SCORE_DATA_CODE";
    public static final int EXIT_CODE = 1;


    //Grille
    private GridView gameGrid = null;
    private int[][] grid = new int [NB_LIGNES][NB_COLONNES];
    private int[][] gridWithFixedBloc = new int [NB_LIGNES][NB_COLONNES];
    private GridAdapter adapter = null;

    //Layouts elements
    private ImageView pauseButton = null;
    private ImageView musicButton = null;
    private TextView scoreText = null;

    //Musique
    private MediaPlayer music = null;

    //Variables du jeu
    private boolean endGame = false;
    private boolean pause = false;
    private int score = 0;

    //Bloc
    private Bloc currentBloc;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game);

        //Recupération des elements du layour
        gameGrid = (GridView)findViewById(R.id.gameGrid);
        scoreText = (TextView)findViewById(R.id.scoreText);
        pauseButton = (ImageView)findViewById(R.id.pauseButton);
        musicButton = (ImageView)findViewById(R.id.musicButton);

        //Gestion de la musique
        music = MediaPlayer.create(this, R.raw.tetris);
        music.start();
        music.setLooping(true);
        music.setVolume(1f,1f);
        music.pause();

        //Génération de la grille de jeu
        generateGrid();

        //Initializing blocs
        currentBloc = Bloc.getRandomBloc();

        //Placement du bloc dans la grille
        placeBloc();

        //start the timer
        startTimer();

        refreshThread();
    }

    @Override
    protected void onPause() {
        super.onPause();
        pause = true;
    }

    @Override
    protected void onResume() {
        super.onResume();
        pause = false;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        finish();
    }

    /**
     * Démarrage du thread du timer
     */
    private void startTimer() {
        final Handler handler = new Handler();
        Runnable r = new Runnable() {
            public void run() {
                if (!pause && !endGame) {
                    continueGame();
                }
                handler.postDelayed(this, GAME_SPEED);
            }
        };
        handler.post(r);
    }

    /**
     * Démarrage du thread du timer
     */
    private void refreshThread() {
        final Handler handler = new Handler();
        Runnable r = new Runnable() {
            public void run() {
                if (!pause && !endGame) {
                    refresh();
                }
                handler.postDelayed(this, 100);
            }
        };
        handler.post(r);
    }

    /**
     * Génération de la grille de jeu
     */
    private void generateGrid() {
        adapter = new GridAdapter(this, NB_COLONNES, NB_LIGNES);
        gameGrid.setNumColumns(NB_COLONNES);
        gameGrid.setAdapter(adapter);
        for (int i = 0; i<NB_LIGNES; i++) {
            for (int j = 0; j<NB_COLONNES; j++) {
                grid[i][j] = 0;
                gridWithFixedBloc[i][j] = 0;
            }
        }
        gameGrid.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View v,
                                    int position, long id) {
                rotate();
            }
        });
    }


    /**
     * Code du jeu : deplacement des pieces
     */
    private void continueGame() {
        if(currentBloc.canMove(Direction.BOTTOM, gridWithFixedBloc)) {
            currentBloc.down();
        }
        else {
            /* SAUVEGARDE DU BLOC DANS LA GRILLE */
            int[][] shape = this.currentBloc.getShape(); //Recupération de la matrice de la piece

            for (int i = 0; i < shape.length; i++) {
                for (int j = 0; j < shape[i].length; j++) {
                    if (shape[i][j] != 0)
                        this.gridWithFixedBloc[this.currentBloc.getPos_j() + i][this.currentBloc.getPos_i() + j] = shape[i][j];
                }
            }

            updateFullLines();

            currentBloc = Bloc.getRandomBloc(); //nouveau bloc
            if(!currentBloc.canMove(Direction.BOTTOM, gridWithFixedBloc)) {
                endGame = true;
                AlertDialog alert = new AlertDialog.Builder(this).setTitle(R.string.app_name)
                        .setMessage(getString(R.string.endGame) + " \n" + getString(R.string.scoreEndText) + ((Integer)score).toString())
                        .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                endGame();
                            }
                        })
                        .show();
                alert.setCanceledOnTouchOutside(false);
            }
        }
    }

    public void endGame() {
        Intent intent = new Intent(GameActivity.this, ScoreActivity.class);
        intent.putExtra(SCORE, ((Integer)score).toString());
        music.stop();
        startActivityForResult(intent, EXIT_CODE);
    }

    /**
     * Place le bloc courant dans la grille
     */
    private void placeBloc() {
        int[][] shape = this.currentBloc.getShape(); //Recupération de la matrice de la piece

        for(int i = 0; i < shape.length; i++) {
            for (int j = 0; j < shape[i].length; j++) {
                if(shape[i][j] != 0)
                    this.grid[this.currentBloc.getPos_j() + i][this.currentBloc.getPos_i() + j] = shape[i][j];
            }
        }
    }

    /**
     * Mise à jour de l'affichage
     */
    private void refresh() {
        clearGrid();
        placeBloc();

        adapter.setGrid(drawGrid());
        adapter.notifyDataSetChanged();
    }


    /**
     * Retourne la grille possédant les bonnes images
     * @return int[]
     */
    public int[] drawGrid() {
        int[] disaplayGrid = new int[this.grid.length * this.grid[0].length];
        for(int i = 0; i < this.grid.length; i++) {
            for (int j = 0; j < this.grid[i].length; j++) {

                int n = i * this.grid[0].length + j;

                switch (grid[i][j]) {
                    case 0:
                        disaplayGrid[n] = R.drawable.empty_bloc;
                        break;
                    case 1:
                        disaplayGrid[n] = R.drawable.bloc_i;
                        break;
                    case 2:
                        disaplayGrid[n] = R.drawable.bloc_j;
                        break;
                    case 3:
                        disaplayGrid[n] = R.drawable.bloc_l;
                        break;
                    case 4:
                        disaplayGrid[n] = R.drawable.bloc_o;
                        break;
                    case 5:
                        disaplayGrid[n] = R.drawable.bloc_s;
                        break;
                    case 6:
                        disaplayGrid[n] = R.drawable.bloc_t;
                        break;
                    case 7:
                        disaplayGrid[n] = R.drawable.bloc_z;
                        break;
                }
            }
        }
        return disaplayGrid;
    }

    /**
     * Clear la grille
     */
    private void clearGrid() {
        for (int i = 0; i<NB_LIGNES; i++) {
            for (int j = 0; j<NB_COLONNES; j++) {
                grid[i][j] = gridWithFixedBloc[i][j];
            }
        }
    }

    private void updateFullLines() {
        boolean full = true;
        int j = 0;
        int nbLineCleared = 0;
        for(int i=0; i<gridWithFixedBloc.length; i++) {
            full = true;
            j = 0;
            while (j < gridWithFixedBloc[i].length && full) {
                if(gridWithFixedBloc[i][j] == 0) {
                    full = false;
                }
                j++;
            }
            if(full) {
                nbLineCleared++;
                clearLine(i);
            }
        }
        if(nbLineCleared > 0) {
            score += nbLineCleared*100 + (50*(nbLineCleared-1)); //100 point par ligne + bonus 50 pour 2lignes effacées
            scoreText.setText("Score : " + ((Integer)score).toString());
            if(GAME_SPEED >= 250)
                GAME_SPEED -= 25*nbLineCleared;
        }
    }

    private void clearLine(int numLine) {
        Log.i("FULL LINE", ((Integer)numLine).toString());
        //decalage de la grid
        while (numLine > 0) {
            for(int j=0; j < gridWithFixedBloc[numLine].length; j++) {
                gridWithFixedBloc[numLine][j] = gridWithFixedBloc[numLine-1][j];
            }
            numLine--;
        }
        //Remise à zero d1e la premiere ligne
        for(int j=0; j < gridWithFixedBloc[numLine].length; j++) {
            gridWithFixedBloc[0][j] = 0;
        }
    }

    /**
     * Mouvement vers la gauche de la pièce
     * @param v
     */
    public void moveLeft(View v) {
        if(currentBloc.canMove(Direction.LEFT, gridWithFixedBloc)) {
            currentBloc.left();
        }
    }

    /**
     * Mouvement vers la droite de la pièce
     * @param v
     */
    public void moveRight(View v) {
        if(currentBloc.canMove(Direction.RIGHT, gridWithFixedBloc)) {
            currentBloc.right();
        }
    }

    /**
     * Mouvement vers le bas de la pièce
     * @param v
     */
    public void moveBottom(View v) {
        if(currentBloc.canMove(Direction.BOTTOM, gridWithFixedBloc)) {
            currentBloc.down();
        }
    }

    /**
     * Rotation de la pièce
     */
    public void rotate() {
        if(currentBloc.canRotate(gridWithFixedBloc)) {
            currentBloc.rotate();
        }
    }

    /**
     * Mouvement vers le bas de la pièce
     * @param v
     */
    public void pauseGame(View v) {
        if(pause){
            pauseButton.setImageResource(R.drawable.pause);
        }
        else {
            pauseButton.setImageResource(R.drawable.play);
        }
        pause = !pause;
    }

    /**
     * Mouvement vers le bas de la pièce
     * @param v
     */
    public void pauseMusic(View v) {
        if(music.isPlaying()){
            music.pause();
            musicButton.setImageResource(R.drawable.music_pause);
        } else {
            music.start();
            musicButton.setImageResource(R.drawable.music_play);
        }
    }
}
