package com.example.gnedelec.tetris.Utils;

import com.example.gnedelec.tetris.R;

/**
 * Created by gnedelec on 14/05/18.
 */

public class Bloc_O extends Bloc {

    Bloc_O() {
        super();
        int[][][] m = {
                //Default
                {
                        {4, 4},
                        {4, 4}
                },
        };
        this.setHeight(2);
        this.setWidth(2);
        this.setMatrix(m);
        this.setColor(R.drawable.bloc_o);
    }
}
