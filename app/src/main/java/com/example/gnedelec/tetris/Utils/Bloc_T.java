package com.example.gnedelec.tetris.Utils;

import com.example.gnedelec.tetris.R;

/**
 * Created by gnedelec on 14/05/18.
 */

public class Bloc_T extends Bloc {

    Bloc_T() {
        super();
        int[][][] m = {
                //Default
                {
                        {0, 6, 0},
                        {6, 6, 6}
                },
                //Other rotation
                {
                        {6, 0},
                        {6, 6},
                        {6, 0}
                },
                {
                        {6, 6, 6},
                        {0, 6, 0}
                },
                {
                        {0, 6},
                        {6, 6},
                        {0, 6}
                },
        };
        this.setHeight(2);
        this.setWidth(3);
        this.setMatrix(m);
        this.setColor(R.drawable.bloc_t);
    }
}
