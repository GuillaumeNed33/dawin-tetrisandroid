package com.example.gnedelec.tetris.Utils;

import com.example.gnedelec.tetris.R;

/**
 * Created by gnedelec on 14/05/18.
 */

public class Bloc_S extends Bloc {

    Bloc_S() {
        super();
        int[][][] m = {
                //Default
                {
                        {0, 5, 5},
                        {5, 5, 0}
                },
                //Other rotation
                {
                        {5, 0},
                        {5, 5},
                        {0, 5}
                },
        };
        this.setHeight(2);
        this.setWidth(3);
        this.setMatrix(m);
        this.setColor(R.drawable.bloc_s);
    }
}
