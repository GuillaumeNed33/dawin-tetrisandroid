package com.example.gnedelec.tetris.Utils;

import com.example.gnedelec.tetris.R;

/**
 * Created by gnedelec on 14/05/18.
 */

public class Bloc_I extends Bloc {

    Bloc_I() {
        super();
        int[][][] m = {
                //Default
                {
                        {1},
                        {1},
                        {1},
                        {1}
                },
                //Other rotation
                {
                        {1, 1, 1, 1}
                }
        };
        this.setHeight(4);
        this.setWidth(1);
        this.setMatrix(m);
        this.setColor(R.drawable.bloc_i);
    }
}
