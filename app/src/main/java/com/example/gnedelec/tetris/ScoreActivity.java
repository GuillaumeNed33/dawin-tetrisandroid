package com.example.gnedelec.tetris;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.example.gnedelec.tetris.Utils.ScoreAdapter;

import java.util.ArrayList;
import java.util.List;

public class ScoreActivity extends AppCompatActivity {

    private ListView listScores;
    private String scorePlayer;
    private SharedPreferences prefs;
    private static final String HIGH_SCORE = "high score";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_score);
        receiveData();

        listScores = (ListView) findViewById(R.id.bestScores);

        displayScores();
    }

    private void receiveData() {
        Intent intent = getIntent();
        scorePlayer = intent.getStringExtra(GameActivity.SCORE);

        if(scorePlayer != null) {
            if (inBestScores()) {
                new AlertDialog.Builder(this).setTitle(R.string.app_name)
                        .setMessage(getString(R.string.highScoreSuccess) + "\n" + getString(R.string.scoreEndText) + scorePlayer + " pts")
                        .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                            }
                        })
                        .show();
            } else {
                new AlertDialog.Builder(this).setTitle(R.string.app_name)
                        .setMessage(getString(R.string.highScoreFail) + "\n" + getString(R.string.scoreEndText) + scorePlayer + " pts")
                        .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                            }
                        })
                        .show();
            }
        }
    }

    public boolean inBestScores() {
        prefs = getSharedPreferences(HIGH_SCORE, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = prefs.edit();
        boolean done = false;
        String score;
        int i = 5;
        boolean res = false;

        if (scorePlayer != null) {
            while (!done && i > 0) {
                score = prefs.getString("score"+((Integer)i).toString(), "0;-1");
                if(Integer.parseInt(scorePlayer) > Integer.parseInt(score.split(";")[1])) {
                    i--;
                } else {
                    done = true;
                }
            }

            if((done && i != 5) || (!done && i == 0)) {
                for(int k=5; k>i+1; k--) {
                    int tmp = k-1;
                    String index = "score"+((Integer)tmp).toString();
                    if(!prefs.getString(index, "0").equals("0"))
                        editor.putString("score"+((Integer)k).toString(), prefs.getString(index, ((Integer)k).toString()+";"));
                }
                int finalPos = i+1;
                String saveScorePlayer = ((Integer)finalPos).toString() + ";" + scorePlayer;
                editor.putString("score"+((Integer)finalPos).toString(), saveScorePlayer);
                editor.apply();
                res = true;
            }
        }
        return res;
    }

    private void displayScores() {
        List<String> scores = new ArrayList<>();
        prefs = getSharedPreferences(HIGH_SCORE, Context.MODE_PRIVATE);
        for(int i=1; i<=5;i++) {
            String scoreTmp = prefs.getString("score" + ((Integer)i).toString(), ((Integer)i).toString()+";Emplacement vide");
            if(!scoreTmp.split(";")[1].equals("Emplacement vide"))
                scoreTmp = scoreTmp.split(";")[0] + ";" + scoreTmp.split(";")[1] + "pts";
            scores.add(scoreTmp);
        }
        ScoreAdapter adapter = new ScoreAdapter(ScoreActivity.this, scores);
        listScores.setAdapter(adapter);
    }

    public void backToMenu(View v) {
        Intent returnIntent = new Intent();
        setResult(Activity.RESULT_CANCELED, returnIntent);
        finish();
    }
}
