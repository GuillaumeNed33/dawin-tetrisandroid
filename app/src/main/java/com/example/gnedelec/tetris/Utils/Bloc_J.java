package com.example.gnedelec.tetris.Utils;

import com.example.gnedelec.tetris.R;

/**
 * Created by gnedelec on 14/05/18.
 */

public class Bloc_J extends Bloc {

    Bloc_J() {
        super();
        int[][][] m = {
                //Default
                {
                        {0,2},
                        {0,2},
                        {2,2}
                },
                //Other rotation
                {
                        {2, 0, 0},
                        {2, 2, 2}
                },
                {
                        {2,2},
                        {2,0},
                        {2,0},
                },
                {
                        {2, 2, 2},
                        {0, 0, 2}
                },
        };
        this.setHeight(3);
        this.setWidth(2);
        this.setMatrix(m);
        this.setColor(R.drawable.bloc_j);
    }
}
