package com.example.gnedelec.tetris.Utils;

import android.content.Context;
import android.support.annotation.Nullable;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;

import com.example.gnedelec.tetris.R;

import java.util.ArrayList;

public class GridAdapter extends BaseAdapter {

    private Context mContext;
    private int nb_cols ;
    private int nb_lines ;
    private int[] mThumbIds;

    public GridAdapter(Context c, int w, int h){

        mContext = c;
        this.nb_cols  = w ;
        this.nb_lines = h ;

        ArrayList<Integer>listTmp = new ArrayList<>();
        for (int i = 0; i < nb_cols; i++) {
            for (int j = 0; j < nb_lines; j++) {
                listTmp.add(R.drawable.empty_bloc);
            }
        }
        mThumbIds = new int[listTmp.size()];
        for(int i = 0; i < listTmp.size(); i++) mThumbIds[i] = listTmp.get(i);
    }

    public void setGrid(int [] grid){
        mThumbIds = grid;
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ImageView imageView;
        if (convertView == null) {
            // if it's not recycled, initialize some attributes
            imageView = new ImageView(mContext);
            imageView.setAdjustViewBounds(true);
            imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
            imageView.setPadding(0, 0, 0, 0);
        } else {
            imageView = (ImageView) convertView;
        }

        imageView.setImageResource(mThumbIds[position]);
        return imageView;
    }

    @Nullable
    @Override
    public CharSequence[] getAutofillOptions() {
        return new CharSequence[0];
    }

    @Override
    public int getCount() {
        return mThumbIds.length;
    }
}
